package com.customamerica.fusiondemo;

/**
 * Created by Bill on 12/18/2014.
 */
class Food {

    private final int id;
    private final double price;
    private final String name;

    Food(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

}
