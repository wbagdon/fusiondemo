package com.customamerica.fusiondemo;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Bill on 12/17/14.
 */
class POSItemAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    // references to our images
    private final Food[] mFood = {
            new Food(R.mipmap.ic_pizza, "Pizza", 2.25),
            new Food(R.mipmap.ic_food, "Hamburger Meal", 11.75),
            new Food(R.mipmap.ic_carrot, "Salad", 7.30),
            new Food(R.mipmap.ic_fish, "Fish", 15.00),
            new Food(R.mipmap.ic_cow, "Steak", 32.00),
            new Food(R.mipmap.ic_candycane, "Dessert", 4.97),
            new Food(R.mipmap.ic_beer, "Beer - Pint", 7.25),
            new Food(R.mipmap.ic_glass_mug, "Beer - Liter", 11.00),
            new Food(R.mipmap.ic_martini, "Martini", 9.50),
            new Food(R.mipmap.ic_glass_flute, "Champagne", 9.00),
            new Food(R.mipmap.ic_glass_tulip, "Wine", 9.00),
            new Food(R.mipmap.ic_coffee, "Coffee", 1.89)
    };

    public POSItemAdapter(LayoutInflater inflater) {
        mInflater = inflater;
    }

    @Override
    public int getCount() {
        return mFood.length;
    }

    @Override
    public Object getItem(int position) {
        return mFood[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Nullable
    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {  // if it's not recycled, initialize some attributes

            convertView = mInflater.inflate(R.layout.pos_item, parent, false);

            holder = new ViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.pos_item_image);
            holder.thumbnailTextView = (TextView) convertView.findViewById(R.id.pos_item_text);

            convertView.setTag(holder);
        } else {
            // skip all the expensive inflation/findViewById
            // and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        holder.thumbnailImageView.setImageResource(mFood[position].getId());
        holder.thumbnailTextView.setText(mFood[position].getName());

        return convertView;
    }

    private static class ViewHolder {
        public ImageView thumbnailImageView;
        public TextView thumbnailTextView;
    }
}
