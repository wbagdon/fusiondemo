package com.customamerica.fusiondemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import custom.android_customfusion_api.CustomFusionUtils;
import custom.android_serialport_api.SerialPort;
import custom.android_serialport_api.SerialPortFinder;
import it.custom.printer.api.android.CustomAndroidAPI;
import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;
import it.custom.printer.api.android.PrinterFont;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final byte[] enableCompression = {0x1b, 0x2a, 0x62, 0x32, 0x4d};
    private static final byte[] setPageSize = {0x1b, 0x26, 0x6c, 0x35, 0x30, 0x52};
    private static final byte[] blackLine = {0x1b, 0x2a, 0x62, 0x33, 0x57, (byte) 0xff, (byte) 0xff, (byte) 0xc0};
    private static final byte[] printGAM = {0x1b, 0x2a, 0x72, 0x42};
    private static final byte[] printSetup = {0x1d, (byte) 0xff, 0x68, 0x30, (byte) 0xb4};
    private static final byte[] openCover = {0x1b, (byte) 0xb4};
    private static final byte[] openDrawer = {0x1b, 0x70, 0x00, 0x00, 0x00};
    private static PrinterFont fontNormal;
    private static PrinterFont fontHeader;
    private static PrinterFont fontRight;
    private final DecimalFormat decimalFormat = new DecimalFormat("#.00");
    private Context mContext;
    private boolean fusionOpen;
    private CustomPrinter prnDevice;
    private CustomFusionUtils customFusionUtils;
    private String printerName;
    private ArrayList<Food> itemOrder;
    private byte[] printLine;

    // Set up some font templates for usage during printing
    private static void setupFonts() {
        fontNormal = new PrinterFont();
        fontHeader = new PrinterFont();
        fontRight = new PrinterFont();
        try {
            fontHeader.setEmphasized(true);
            fontHeader.setJustification(PrinterFont.FONT_JUSTIFICATION_CENTER);
            fontRight.setJustification(PrinterFont.FONT_JUSTIFICATION_RIGHT);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this.getApplicationContext();

        // create grid of POS items
        GridView gridView = (GridView) findViewById(R.id.item_grid);
        gridView.setAdapter(new POSItemAdapter(getLayoutInflater()));
        gridView.setOnItemClickListener(this);

        // attach print button
        Button printButton = (Button) findViewById(R.id.print_button);
        printButton.setOnClickListener(this);

        // verify that this device is a Fusion
        if (checkFusionExists()) {
            // create the CustomPrinter object for print / display
            openFusion();
            // create the Fusion object for beep / drawer / cover
            customFusionUtils = new CustomFusionUtils();
            // create a line byte array for printing lines
            createLineByteArray();
            // setup the fonts for printing
            setupFonts();
            // set printer name string to Fusion
            printerName = prnDevice.getPrinterName();
            // write welcome message to the display
            updateDisplay("   Custom America", "FUSION       ");
        }

        // setup ArrayList for item orders
        itemOrder = new ArrayList<>();
        itemOrder.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_beep) {
            // beep for 500 ms
            beep();
        } else if (id == R.id.action_print_setup) {
            // print the setup / settings page from the printer
            printSetup();
        } else if (id == R.id.action_cover_open) {
            // open the cover to change paper
            openCover();
        } else if (id == R.id.action_drawer_kick) {
            openCashDrawer();
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean checkFusionExists() {
        String[] availableComPorts = new SerialPortFinder().getAllDevicesPath();

        //setup for Fusion device
        String fusionComDevice = mContext.getString(R.string.fusion_com_port);
        boolean fusionExists = false;

        if (availableComPorts != null) {
            //search for Fusion COM port
            for (String availableComPort : availableComPorts) {
                if (availableComPort.equals(fusionComDevice)) {
                    fusionExists = true;
                }
            }
            if (fusionExists) {
                try {
                    SerialPort sp;
                    //Open and set the configuration object
                    try {
                        //Set com port parameters
                        sp = new SerialPort(new File(fusionComDevice), 115200, SerialPort.NON_BLOCKING);
                        sp.setParameters(SerialPort.DATABITS_8, SerialPort.PARITY_NONE, SerialPort.STOPBITS_1, SerialPort.FLOWCONTROL_RTSCTS);
                    } catch (Exception ex) {
                        sp = null;
                    }
                    //Close the configuration object
                    if (sp != null) {
                        Log.d("COMPORT", "Closed COM Port");
                        sp.Close();
                    }
                } catch (Exception e) {
                    //Log Error
                    Log.d("Protocol", "COM-" + e.toString());
                    fusionExists = false;
                }
            } else {
                //Show Error
                Snackbar.make(this.getCurrentFocus(), "Fusion not found", Snackbar.LENGTH_SHORT)
                        .show();
                fusionExists = false;
            }
        } else {
            //Show Error
            Snackbar.make(this.getCurrentFocus(), "Fusion not found", Snackbar.LENGTH_SHORT)
                    .show();
            fusionExists = false;
        }

        return fusionExists;
    }

    private void openFusion() {
        // if the device is open, close then try to open communication again
        if (fusionOpen) {
            try {
                prnDevice.close();
                fusionOpen = false;
            } catch (CustomException e) {
                e.printStackTrace();
            }
        }

        try {
            prnDevice = new CustomAndroidAPI().getPrinterDriverCOM(mContext.getString(R.string.fusion_com_port));
            fusionOpen = true;
        } catch (CustomException e) {
            e.printStackTrace();
        }

    }

    private void print() {
        try {
            // print logo at top of page
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.custom_logo);
            prnDevice.printImage(bm, CustomPrinter.IMAGE_ALIGN_TO_CENTER, CustomPrinter.IMAGE_SCALE_TO_WIDTH, 300);
            prnDevice.feed(1);

            // print address of company
            prnDevice.printTextLF(getString(R.string.address1), fontHeader);
            prnDevice.printTextLF(getString(R.string.address2), fontHeader);
            prnDevice.printTextLF(getString(R.string.phone), fontHeader);
            prnDevice.feed(1);

            // print the printer name
            prnDevice.printText(getString(R.string.ticket_printer_name_text) + " ", fontNormal);
            prnDevice.printTextLF(printerName, fontNormal);

            // add black line
            prnDevice.writeData(printLine);
            prnDevice.feed(1);

            // print each item from the order
            double totalPrice = 0;
            for (int i = 0; i < itemOrder.size(); i++) {
                prnDevice.printTextLF(itemOrder.get(i).getName(), fontNormal);
                prnDevice.printTextLF("$" + decimalFormat.format(itemOrder.get(i).getPrice()), fontRight);
                totalPrice += itemOrder.get(i).getPrice();
            }

            // print a black line
            prnDevice.writeData(printLine);
            prnDevice.feed(1);

            // print the total cost
            prnDevice.printTextLF("Total:", fontNormal);
            prnDevice.printTextLF("$" + decimalFormat.format(totalPrice), fontRight);
            prnDevice.feed(1);

            //display the total cost
            updateDisplay("TOTAL:", "$" + decimalFormat.format(totalPrice));

            // add text to explain QR-code
            prnDevice.printTextLF(getString(R.string.ticket_scan_text), fontHeader);
            prnDevice.feed(1);

            // print QR-code to website
            String url = getString(R.string.custom_url);
            prnDevice.printBarcode2D(url, CustomPrinter.BARCODE_TYPE_QRCODE, CustomPrinter.BARCODE_ALIGN_TO_CENTER, 150);
            prnDevice.feed(4);

            // perform a partial cut
            prnDevice.cut(CustomPrinter.CUT_PARTIAL);

        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    // print the settings page from the printer
    private void printSetup() {
        try {
            prnDevice.writeData(printSetup);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    // open the cover to change the roll
    private void openCover() {
        try {
            prnDevice.writeData(openCover);
        } catch (CustomException e) {
            e.printStackTrace();
        }

    }

    // open the cover to change the roll
    private void openCashDrawer() {
        try {
            prnDevice.writeData(openDrawer);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    private void beep() {
        customFusionUtils.BuzzerBeep(500);
    }

    // create a black line using the GAM commands
    private void createLineByteArray() {
        //concatenate the GAM commands to make a line
        //this lessens the amount of times to open the pipe to the printer
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            outputStream.write(setPageSize);
            outputStream.write(enableCompression);
            outputStream.write(blackLine);
            outputStream.write(blackLine);
            outputStream.write(printGAM);
        } catch (IOException e) {
            e.printStackTrace();
        }

        printLine = outputStream.toByteArray();
    }

    // basic command to manage the customer display
    // top line is left aligned, bottom line is right aligned
    private void updateDisplay(String line1, String line2) {
        if (line1.length() < 20 && line2.length() < 20) {
            try {
                byte[] buffer = new byte[42];
                buffer[0] = 0x1b;
                buffer[1] = (byte) 0xb3;
                for (int i = 2; i < 42; i += 1) {
                    buffer[i] = ' ';
                }
                byte[] bLine1 = line1.getBytes("UTF-8");
                byte[] bLine2 = line2.getBytes("UTF-8");
                for (int i = 0; i < bLine1.length; i += 1) {
                    buffer[i + 2] = bLine1[i];
                }
                int j = bLine2.length;
                for (int i = 41; i > 41 - bLine2.length; i -= 1) {
                    buffer[i] = bLine2[j -= 1];
                }
                prnDevice.writeData(buffer);
            } catch (CustomException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    // button to complete the order (if there is at least one item)
    @Override
    public void onClick(View v) {
        if (itemOrder.size() <= 0) {
            Snackbar.make(this.getCurrentFocus(), "Enter at least one item", Snackbar.LENGTH_SHORT)
                    .show();
        } else {
            beep();
            openFusion();
            print();
            openCashDrawer();
            itemOrder.clear();
        }
    }

    // event handler for POS items
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        customFusionUtils.BuzzerBeep(100);

        Food tempFood = (Food) parent.getItemAtPosition(position);
        Snackbar.make(findViewById(R.id.linearLayout),
                tempFood.getName() + " added to the order!",
                Snackbar.LENGTH_SHORT)
                .show();
        updateDisplay(tempFood.getName(), "$" + decimalFormat.format(tempFood.getPrice()));

        itemOrder.add(tempFood);
    }
}
