# README #

### What is this repository for? ###

* Quick demo utility for Android image on Custom FUSION

### How do I get set up? ###

* Install the latest Android Studio
* Download latest Jetbrains BitBucket connector
* [https://bitbucket.org/dmitry_cherkas/jetbrains-bitbucket-connector](https://bitbucket.org/dmitry_cherkas/jetbrains-bitbucket-connector)

### Contribution guidelines ###

* If you would like to see any changes or find an issue, please report to the issues page